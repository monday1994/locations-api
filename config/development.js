module.exports = {
    server: {
        baseUrl: 'localhost',
        port: 3000,
    },
    database: {
        name: 'locations_db'
    }
};