require('dotenv').load();

const util = require('util');
const koa = require('koa');
const logger = require('koa-logger');
const swagger = require('swagger-koa');
const router = require('./src/api/middlewares/main-router');
const globalErrorHandler = require('./errorHandler').globalErrorHandler;
const cors = require('koa-cors');

const app = new koa();

//logger
app.use(logger());
app.use(cors());

//swagger
app.use(swagger.init({
    apiVersion: '1.0',
    swaggerVersion: '2.0',
    swaggerURL: '/swagger',
    swaggerJSON: '/api-docs.json',
    swaggerUI: './public/swagger/',
    basePath: 'http://localhost:3000',
    info: {
        title: 'swagger-koa sample app',
        description: 'Swagger + Koa = {swagger-koa}'
    },
    apis: ['./src/api/middlewares/main/main-middleware.js']
}));

// error handler
app.use(globalErrorHandler);

//routes init
app.use(router.middleware());
app.use(router.routes());
app.use(router.allowedMethods());

//db init
require('./src/database/index');

app.listen(3000);
console.log("server listens on ", 3000);

//tests
(async () => {
    let moment = require('moment');
   /* let r = await require('./src/database/repositories/users-repo').findLocationsInRange({
        identifier: 'H1nMbqicG',
        startDate: moment('2018-05-01T12:20'),
        endDate: moment('2018-05-01T12:40')
    });
    console.log("r = ", util.inspect(r));*/
})();
