module.exports = {
    onlyAlphabetLetterRegex : /^[a-zA-ZĄĆĘŁŃÓŚŹŻąćęłńóśźż\s]+$/,
    onlyNumbersRegex : /^[0-9]+$/,
    emailRegex : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,
    birthdayRegex : /[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/,
    coordinatesRegex : /^(\-?\d+(\.\d+)?).\s*(\-?\d+(\.\d+)?)$/,
    hashTagRegex : /^#\S+$/,
    allCharactersUsedInWriting : /^[a-zA-Z0-9ĄĆĘŁŃÓŚŹŻąćęłńóśźż!@#$*_/.\-,\s]+$/,
    polishPostalCode : /[0-9]{2}\-[0-9]{3}/,
    accessToken : /^[a-zA-Z0-9]+$/,
    alphaNumeric : /^[a-z0-9]+$/
};