const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const regex = require('../constants/regex');
const VP = require('../constants/validation-parameters');

const userSchema = new mongoose.Schema({
    identifier : {
        type : String,
        required : true,
        minlength : VP.IDENTIFIER_MIN_LENGTH,
        maxlength: VP.IDENTIFIER_MAX_LENGTH},
    nickname : {
        type: String,
        required : true,
        match : regex.allCharactersUsedInWriting,
        minlength : VP.NICKNAME_MIN_LENGTH,
        maxlength : VP.NICKNAME_MAX_LENGTH
    },
    password : {
        type : String,
        required : true,
        match : regex.allCharactersUsedInWriting,
        minlength : VP.PASSWORD_MIN_LENGTH,
        maxlength : VP.PASSWORD_MAX_LENGTH,
        lowercase : true
    },
    locations : [{
        latitude: { type: Number },
        longitude: { type: Number },
        date: { type: String }
    }]
});

userSchema.pre('update', function(next) {
    this.options.runValidators = true;
    next();
});

exports.users = mongoose.model('users', userSchema);
