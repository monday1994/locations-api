const config = require('../../config/development');
const mongoose = require('mongoose');
const schemas = require('./user-schema');
mongoose.connect(`mongodb://${config.server.baseUrl}/${config.database.name}`);

mongoose.Promise = global.Promise;

const db = mongoose.connection;

//adding collections to database
db.users = schemas.users;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    console.log("connected to database...");
});

module.exports = db;
