const db = require('../../database');
const mongoose = require("mongoose");
const util = require('util');
const Boom = require('boom');
const moment = require('moment');

const getByIdentifier = identifier => {
    return db.users.find({identifier});
};

exports.getByIdentifier = getByIdentifier;

exports.getByNickname = nickname => {
    return db.users.find({"nickname" : nickname});
};

exports.doesUserExists = async (nickname, password) => {
    let doesUserExist = await db.users.findOne({
        'nickname' : nickname,
        'password': password
    });
    console.log("does user exist = ", util.inspect(doesUserExist));
    return !!doesUserExist;
};

exports.validateLogin = async (nickname, password) => {
    return await db.users.findOne({
        'nickname' : nickname,
        'password': password
    });
};

exports.create = newUser => {
    return db.users.create(newUser);
};

const update = userToBeUpdate => {
    let { identifier } = userToBeUpdate;

    return db.users.update({
        "identifier" : identifier
    },{
        $set: userToBeUpdate
    },{"new" : true});
};

exports.update = update;

exports.addLocation = async (identifier, location) => {
    let [ user ] = await getByIdentifier(identifier);

    if(user){
        console.log("user = ", user);
        user.locations.push(location);
        return update(user);
    }

    return null;
};

exports.findLocationsInRange = async ({ identifier, startDate, endDate }) => {

    const [ user ] = await getByIdentifier(identifier);

    if(user){
        user.locations = user.locations.filter(location => {
            const date = moment(location.date);
            return startDate.diff(date) <= 0 && endDate.diff(date) >= 0;
        });

        return user;
    }

    return null;
};

exports.remove = identifier => {
    return db.remove({"identifier": identifier});
};
