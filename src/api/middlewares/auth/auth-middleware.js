const Boom = require('boom');
const shortId = require('shortid');
const usersRepo = require('../../../database/repositories/users-repo');
const errorHandler = require('../../../../errorHandler').errorHandler;

const registration = () => {
    return async (ctx, next) => {
        //await next();
        console.log("jestem");
        let { nickname, password } = ctx.request.body;

        let newUser = {
            identifier: shortId.generate(),
            nickname,
            password,
            locations: []
        };

        let createdUser = await usersRepo.create(newUser);
        console.log("created user = ", createdUser);
        ctx.body = {
            newUser: createdUser
        }
    }
};

const login = () => {
    return async (ctx, next) => {
        let { nickname, password} = ctx.request.body;

        const user = await usersRepo.validateLogin(nickname, password);

        if(user){
            ctx.body = {
                user: user
            }
        } else {
            errorHandler(ctx, 'Wrong login or password', 401);
        }

    }
};

module.exports = {
    registration,
    login
};
