const Boom = require('boom');
const Router = require('koa-joi-router');
const middlewares = require('./auth-middleware');
const VP = require('../../../constants/validation-parameters');

const Joi = Router.Joi;
const router = new Router();

router.route({
    method: 'post',
    path: '/login',
    validate: {
        body: {
            nickname: Joi.string().min(VP.NICKNAME_MIN_LENGTH).max(VP.NICKNAME_MAX_LENGTH).required(),
            password: Joi.string().min(VP.PASSWORD_MIN_LENGTH).max(VP.PASSWORD_MAX_LENGTH).required()
        },
        type: 'json'
    },
    handler: middlewares.login()
});

router.route({
    method: 'post',
    path: '/registration',
    validate: {
        body: {
            nickname: Joi.string().min(VP.NICKNAME_MIN_LENGTH).max(VP.NICKNAME_MAX_LENGTH).required(),
            password: Joi.string().min(VP.PASSWORD_MIN_LENGTH).max(VP.PASSWORD_MAX_LENGTH).required()
        },
        type: 'json'
    },
    handler: middlewares.registration()
});

module.exports = router;