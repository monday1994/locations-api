const util = require('util');
const usersRepo = require('../../../database/repositories/users-repo');
const moment = require('moment');
const boom = require('boom');
const errorHandler = require('../../../../errorHandler').errorHandler;

/**
 * @swagger
 * resourcePath: /api
 * description: All about API
 */

/**
 * @swagger
 * path: /login
 * operations:
 *   -  httpMethod: POST
 *      summary: Login with username and password
 *      notes: Returns a user based on username
 *      responseClass: User
 *      nickname: login
 *      consumes:
 *        - text/html
 *      parameters:
 *        - name: username
 *          description: Your username
 *          paramType: query
 *          required: true
 *          dataType: string
 *        - name: password
 *          description: Your password
 *          paramType: query
 *          required: true
 *          dataType: string
 */

const index = () => {
    return async (ctx, next) => {

        ctx.type = 'application/json';
        ctx.body = {
            status : 'server works properly'
        };
    }
};

/**
 * @swagger
 * models:
 *   User:
 *     id: User
 *     properties:
 *       username:
 *         type: String
 *       password:
 *         type: String
 */

const saveLocation = () => {
    return async (ctx, next) => {
        let { latitude, longitude, date, identifier } = ctx.request.body;

        let newLocation = {
            latitude,
            longitude,
            date: moment(date).format()
        };

        const result = await usersRepo.addLocation(identifier, newLocation);

        if(result){
            ctx.body = {
                result : result
            };
        } else {
            errorHandler(ctx, `User ${identifier} does not exist`, 400);
        }
    }
};

const getLocationsInRange = () => {
    return async (ctx, next) => {
        let { identifier, startDate, endDate } = ctx.request.query;

        const locationRange = {
            identifier,
            startDate: moment(startDate),
            endDate: moment(endDate)
        };

        const result = await usersRepo.findLocationsInRange(locationRange);

        if(result){
           // ctx.response.headers['Access-Control-Allow-Origin'] = 'http://localhost:3001';
           ctx.response.headers['Access-Control-Allow-Origin'] = '*';

            ctx.body = {
                result : result
            };
        } else {
            errorHandler(ctx, `User ${identifier} does not exist`, 400);
        }
    }
};

module.exports = {
    index,
    saveLocation,
    getLocationsInRange
};
