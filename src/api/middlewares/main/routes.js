const Boom = require('boom');
const Router = require('koa-joi-router');
const middlewares = require('./main-middleware');
const VP = require('../../../constants/validation-parameters');
const Joi = Router.Joi;
const router = new Router();

router.route({
    method: 'get',
    path: '/',
    handler: middlewares.index()
});

router.route({
    method: 'post',
    path: '/save-location',
    validate: {
        body: {
            identifier: Joi.string().min(VP.IDENTIFIER_MIN_LENGTH).max(VP.IDENTIFIER_MAX_LENGTH).required(),
            latitude: Joi.number().min(-90.0).max(90.0).required(),
            longitude: Joi.number().min(-180.0).max(180.0).required(),
            date: Joi.date().min('now').required()
        },
        type: 'json'
    },
    handler: middlewares.saveLocation()
});

router.route({
    method: 'get',
    path: '/locations-in-range',
    validate: {
        query: {
            identifier: Joi.string().min(VP.IDENTIFIER_MIN_LENGTH).max(VP.IDENTIFIER_MAX_LENGTH).required(),
            startDate: Joi.date().required(),
            endDate: Joi.date().required()
        },
    },
    handler: middlewares.getLocationsInRange()
});

module.exports = router;