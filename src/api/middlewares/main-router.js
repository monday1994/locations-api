const RouterFactory = require('koa-router');
const mainMiddlewares = require('./main/routes');
const authMiddlewares = require('./auth/routes');

const mainRouter = new RouterFactory();

mainRouter.use('', mainMiddlewares.middleware());
mainRouter.use('/auth', authMiddlewares.middleware());

module.exports = mainRouter;
