const util = require('util');
const boom = require('boom');

const globalErrorHandler = async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        console.log("err = ", util.inspect(err));
        let {
            status,
            details:
                [
                    message = {}
                ] = []
        } = err;


        console.log("destructured status = ", status);
        console.log("message destructured from details = ", message);

        ctx.status = status || 500;

        ctx.body = {
            error: message
        };
    }
};

const errorHandler = (ctx, message, status) => {
    ctx.status = status;
    switch (status) {
        case 400:
            ctx.body = {
                ...boom.badRequest(message).output.payload
            };
            break;
        case 401:
            ctx.body = {
                ...boom.unauthorized(message).output.payload
            };
            break;
        default:
            ctx.body = {
                error: message
            };
            break;
    }
};



module.exports = {
    globalErrorHandler,
    errorHandler
};